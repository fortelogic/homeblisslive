<?php
// include the API
header('Access-Control-Allow-Origin: *');
require('cloudfiles/cloudfiles.php');

// cloud info
$username = "zansaar"; // username
$key = "08e700e465ee007261400742ab14a3a4"; // api key

// Connect to Rackspace
$auth = new CF_Authentication($username, $key);
$auth->authenticate();
$conn = new CF_Connection($auth);

// Get the container we want to use
$container = $conn->get_container('homebliss');

// store file information
$localfile = $_FILES['file']['tmp_name'];
$filename = $_POST['name'];

// upload file to Rackspace
$object = $container->create_object($filename);
$object->load_from_filename($localfile);

echo json_encode($object);
?>